from typing import Iterable


def first_recurring_in_list(list_in: Iterable):
    presence_dict = dict()
    for el in list_in:
        if el in presence_dict:
            return el
        else:
            presence_dict[el] = True
    return None


if __name__ == "__main__":
    list_in = [1, 3, 4, 5, 1, 2, 5]
    print(f"First recurring character in {list_in} is {first_recurring_in_list(list_in)}")

    list_in = [1, 3, 4, 5]
    print(f"First recurring character in {list_in} is {first_recurring_in_list(list_in)}")

    list_in = []
    print(f"First recurring character in {list_in} is {first_recurring_in_list(list_in)}")

    list_in = "hello"
    print(f"First recurring character in {list_in} is {first_recurring_in_list(list_in)}")
