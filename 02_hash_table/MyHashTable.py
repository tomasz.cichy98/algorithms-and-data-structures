from typing import Any, List, Optional


class MyHashTable:
    """
    Keys can only be strings. Because it is easy to hash them. This hash table will have collisions.
    """

    def __init__(self, size: int):
        self.data = [None] * size

    def _hash(self, key: str) -> int:
        hash = 0
        for i in range(len(key)):
            hash = (hash + ord(key[i]) * i) % len(self.data)
        return hash

    def set(self, key: str, value: Any):
        address = self._hash(key)
        # having arrays on address will help with collisions
        if not self.data[address]:
            self.data[address] = []
        self.data[address].append([key, value])

    def get(self, key: str) -> Optional[Any]:
        address = self._hash(key)
        bucket = self.data[address]

        if bucket:
            for item in bucket:
                if item[0] == key:
                    return item[1]
        return None

    def keys(self) -> List[str]:
        # in Python 3 dict.keys() is O(1), here it is O(n)
        return [item[0] for bucket in self.data for item in bucket if bucket]


if __name__ == "__main__":
    my_hash_table = MyHashTable(size=3)
    print(f"Hashing 'hello', result: {my_hash_table._hash(key='hello')}")

    my_hash_table.set(key="hello", value="world")
    my_hash_table.set(key="grapes", value="sweet")
    # apples and oranges will be in the same bucket when size=3
    my_hash_table.set(key="apples", value="tasty")
    my_hash_table.set(key="oranges", value="juicy")
    print(my_hash_table.data)
    print(f"Get without collision: `hello` - {my_hash_table.get('hello')}")
    print(f"Get with collision: `apples` - {my_hash_table.get('apples')}")
    print(f"Get with collision: `oranges` - {my_hash_table.get('oranges')}")

    print(f"All keys in the table: {my_hash_table.keys()}")
