def merge_sort(l_in: list):
    if len(l_in) > 1:
        # split into left and right
        middle = len(l_in) // 2
        left = l_in[:middle]
        right = l_in[middle:]

        # sort halves
        merge_sort(left)
        merge_sort(right)

        i = j = k = 0

        # pick larger among the elements of left and right and place in correct position
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                l_in[k] = left[i]
                i += 1
            else:
                l_in[k] = right[j]
                j += 1
            k += 1

        # when run out of elements in either list, pick the remaining elements and put in a right place
        while i < len(left):
            l_in[k] = left[i]
            i += 1
            k += 1
        while j < len(right):
            l_in[k] = right[j]
            j += 1
            k += 1


if __name__ == "__main__":
    numbers = [99, 12, 3, 5, 0, -5]
    merge_sort(numbers)
    print(numbers)
