def insertion_sort(l_in: list) -> list:
    for i in range(1, len(l_in)):
        key = l_in[i]
        j = i - 1
        # compare with each element on the left
        while j >= 0 and key < l_in[j]:
            l_in[j + 1] = l_in[j]
            j -= 1
        # place the value after the last elements smaller the value
        l_in[j + 1] = key
    return l_in


if __name__ == "__main__":
    numbers = [99, 12, 3, 5, 0, -5]
    print(insertion_sort(numbers))
