def selection_sort(l_in: list) -> list:
    for i in range(len(l_in)):
        curr_min_index = i
        curr_min_el = l_in[i]
        for j in range(i + 1, len(l_in)):
            # look for a number lower than a current min
            if l_in[j] < curr_min_el:
                curr_min_index = j
        l_in[i] = l_in[curr_min_index]
        l_in[curr_min_index] = curr_min_el
    return l_in


if __name__ == "__main__":
    numbers = [99, 12, 3, 5, 0, -5]
    print(selection_sort(numbers))
