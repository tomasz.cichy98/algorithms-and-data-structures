def bubble_sort(l_in: list) -> list:
    for i in range(len(l_in)):
        for j in range(len(l_in) - i - 1):
            if l_in[j] > l_in[j + 1]:
                # swap numbers
                l_in[j], l_in[j + 1] = l_in[j + 1], l_in[j]
    return l_in


if __name__ == "__main__":
    numbers = [99, 12, 3, 5, 0, -5]
    print(bubble_sort(numbers))
