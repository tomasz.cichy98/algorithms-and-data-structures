class Node:
    def __init__(self, value):
        self.left = None
        self.right = None
        self.value = value


class MyBinarySearchTree:
    def __init__(self):
        self.root = None

    def insert(self, value):
        new_node = Node(value)
        if not self.root:
            self.root = new_node
        else:
            parent = self.root
            while True:
                if value > parent.value:
                    if parent.right:
                        parent = parent.right
                    else:
                        parent.right = new_node
                        return
                else:
                    if parent.left:
                        parent = parent.left
                    else:
                        parent.left = new_node
                        return

    def lookup(self, value):
        if not self.root:
            return False
        current_node = self.root
        while current_node:
            if value > current_node.value:
                current_node = current_node.right
            elif value < current_node.value:
                current_node = current_node.left
            else:
                return current_node
        return False

    def bfs(self) -> list:
        current_node = self.root
        out = []
        queue = [current_node]

        while queue:
            current_node = queue.pop(0)  # this is O(n), we can use a real queue to fix it
            out.append(current_node.value)
            if current_node.left:
                queue.append(current_node.left)
            if current_node.right:
                queue.append(current_node.right)
        return out

    def bfs_recursive(self, queue: list, out: list):
        if not queue:
            return out
        current_node = queue.pop(0)
        out.append(current_node.value)
        if current_node.left:
            queue.append(current_node.left)
        if current_node.right:
            queue.append(current_node.right)
        return self.bfs_recursive(queue=queue, out=out)

    def dfs_in_order(self):
        return traverse_in_order(node=self.root, data=[])

    def dfs_pre_order(self):
        return traverse_pre_order(node=self.root, data=[])

    def dfs_post_order(self):
        return traverse_post_order(node=self.root, data=[])

    def is_bst(self, node, min_val=float("-inf"), max_val=float("inf")) -> bool:
        if node.value <= min_val:
            return False

        if node.value >= max_val:
            return False

        left_ok = right_ok = True

        if node.left:
            left_ok = self.is_bst(node.left, min_val, node.value)

        if node.right:
            right_ok = self.is_bst(node.right, node.value, max_val)

        return left_ok and right_ok


def traverse_in_order(node: Node, data: list):
    if node.left:
        traverse_in_order(node=node.left, data=data)
    data.append(node.value)
    if node.right:
        traverse_in_order(node.right, data=data)
    return data


def traverse_pre_order(node: Node, data: list):
    data.append(node.value)
    if node.left:
        traverse_pre_order(node=node.left, data=data)
    if node.right:
        traverse_pre_order(node.right, data=data)
    return data


def traverse_post_order(node: Node, data: list):
    if node.left:
        traverse_post_order(node=node.left, data=data)
    if node.right:
        traverse_post_order(node.right, data=data)
    data.append(node.value)
    return data


def print_tree(node, level=0):
    if node:
        print_tree(node.right, level + 1)
        print(' ' * 4 * level + '-> ' + str(node.value))
        print_tree(node.left, level + 1)


if __name__ == "__main__":
    my_bst = MyBinarySearchTree()
    my_bst.insert(9)
    my_bst.insert(4)
    my_bst.insert(6)
    my_bst.insert(20)
    my_bst.insert(170)
    my_bst.insert(15)
    my_bst.insert(1)
    print_tree(my_bst.root)
    print(my_bst.lookup(9))
    print(my_bst.lookup(170))
    print(my_bst.lookup(-1))

    print("BFS: ", my_bst.bfs())
    print("BFS recursive: ", my_bst.bfs_recursive(queue=[my_bst.root], out=[]))

    print("DFS in order: ", my_bst.dfs_in_order())
    print("DFS pre order: ", my_bst.dfs_pre_order())
    print("DFS post order: ", my_bst.dfs_post_order())
    print("Validate tree: ", my_bst.is_bst(my_bst.root))
