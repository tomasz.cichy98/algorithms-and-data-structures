def binary_search_recursive(data: list, target: int, low: int = None, high: int = None) -> int:
    if high >= low:
        middle = low + (high - low) // 2
        if data[middle] == target:
            return middle
        elif data[middle] < target:
            return binary_search_recursive(data=data, target=target, low=middle + 1, high=high)
        else:
            return binary_search_recursive(data=data, target=target, low=low, high=middle - 1)
    else:
        return -1


def binary_search_iterative(data: list, target: int) -> int:
    low = 0
    high = len(data) - 1
    while low <= high:
        middle = low + (high - low) // 2
        if data[middle] == target:
            return middle
        elif data[middle] < target:
            low = middle + 1
        else:
            high = middle - 1
    return -1


if __name__ == "__main__":
    numbers = [1, 4, 6, 10]
    print(binary_search_recursive(data=numbers, target=4, low=0, high=len(numbers) - 1))
    print(binary_search_recursive(data=numbers, target=3, low=0, high=len(numbers) - 1))
    print(binary_search_iterative(data=numbers, target=4))
    print(binary_search_iterative(data=numbers, target=3))
