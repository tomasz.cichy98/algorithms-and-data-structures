class Node:
    def __init__(self, value):
        self.value = value
        self.next = None


class MyStackLinkedList:
    def __init__(self):
        self.top = None
        self.bottom = None
        self.length = 0

    def peek(self):
        return self.top.value if self.top else None

    def push(self, value):
        new_node = Node(value)
        if self.length == 0:
            self.top = new_node
            self.bottom = new_node
        else:
            hold_pointer = self.top
            self.top = new_node
            self.top.next = hold_pointer
        self.length += 1

    def pop(self):
        if not self.top:
            return None
        if self.top == self.bottom:
            self.bottom = None
        hold_pointer = self.top
        self.top = self.top.next
        self.length -= 1
        return hold_pointer.value


if __name__ == "__main__":
    my_stack = MyStackLinkedList()
    print(my_stack.peek())
    my_stack.push("google.com")
    print(my_stack.peek())
    my_stack.push("reddit.com")
    print(my_stack.peek())
    print(my_stack.pop())  # reddit.com
    print(my_stack.pop())  # google.com
    print(my_stack.pop())  # None
