from MyStackArray import MyStackArray


class MyQueueTwoStacks:
    def __init__(self):
        self.in_stack = MyStackArray()
        self.hold_stack = MyStackArray()

    def enqueue(self, value):
        # move everything to hold stack
        while len(self.in_stack):
            self.hold_stack.push(self.in_stack.pop())

        # add to in stack
        self.in_stack.push(value)

        # move everything back to in stack
        while len(self.hold_stack):
            self.in_stack.push(self.hold_stack.pop())

    def dequeue(self):
        if not self.in_stack:
            return None
        return self.in_stack.pop()

    def peek(self):
        return self.in_stack.peek()


if __name__ == "__main__":
    my_queue = MyQueueTwoStacks()
    print(my_queue.peek())
    my_queue.enqueue("Jon")
    my_queue.enqueue("Tomasz")
    my_queue.enqueue("Alpaca")
    print(my_queue.peek())  # Jon
    print(my_queue.dequeue())  # Jon
    print(my_queue.dequeue())  # Tomasz
    print(my_queue.dequeue())  # Alpaca
    print(my_queue.dequeue())  # None
