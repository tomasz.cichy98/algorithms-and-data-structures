class Node:
    def __init__(self, value):
        self.value = value
        self.next = None


class MyQueue:
    def __init__(self):
        self.first = None
        self.last = None
        self.length = 0

    def peel(self):
        return self.first.value if self.first else None

    def enqueue(self, value):
        new_node = Node(value)
        if self.length == 0:
            self.first = new_node
            self.last = new_node
        else:
            self.last.next = new_node
            self.last = new_node
        self.length += 1

    def dequeue(self):
        if not self.first:
            return None
        if self.first == self.last:
            self.last = None
        hold_pointer = self.first
        self.first = self.first.next
        self.length -= 1
        return hold_pointer.value


if __name__ == "__main__":
    my_queue = MyQueue()
    print(my_queue.peel())
    my_queue.enqueue("Jon")
    my_queue.enqueue("Tomasz")
    my_queue.enqueue("Alpaca")
    print(my_queue.peel())
    print(my_queue.dequeue())  # Jon
    print(my_queue.dequeue())  # Tomasz
    print(my_queue.dequeue())  # Alpaca
    print(my_queue.dequeue())  # None
