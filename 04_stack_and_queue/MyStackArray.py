class MyStackArray:
    def __init__(self):
        self.data = []

    def peek(self):
        return self.data[-1] if self.data else None

    def push(self, value):
        self.data.append(value)

    def pop(self):
        return self.data.pop() if self.data else None

    def __len__(self):
        return len(self.data)


if __name__ == "__main__":
    my_stack = MyStackArray()
    print(my_stack.peek())
    my_stack.push("google.com")
    print(my_stack.peek())
    my_stack.push("reddit.com")
    print(my_stack.peek())
    print(my_stack.pop())  # reddit.com
    print(my_stack.pop())  # google.com
    print(my_stack.pop())  # None
