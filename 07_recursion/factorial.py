def factorial_recursive(number):
    if number < 2:
        return number
    return number * factorial_recursive(number - 1)


def factorial_iterative(number):
    out = 1
    for i in range(2, number + 1):
        out *= i
    return out


if __name__ == "__main__":
    print(factorial_iterative(2))
    print(factorial_iterative(5))
    print(factorial_iterative(10))
    print(factorial_recursive(2))
    print(factorial_recursive(5))
    print(factorial_recursive(10))
