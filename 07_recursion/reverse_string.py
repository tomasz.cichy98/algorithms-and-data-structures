def reverse_string_recursive(s_in: str) -> str:
    if s_in == "":
        return s_in
    return reverse_string_recursive(s_in[1:]) + s_in[0]


if __name__ == "__main__":
    print(reverse_string_recursive("hello"))
