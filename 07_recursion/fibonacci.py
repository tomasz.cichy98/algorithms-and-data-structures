# 0 1 1 2 3 5 8
def fibonacci_recursive(n):
    if n <= 1:
        return n
    return fibonacci_recursive(n - 2) + fibonacci_recursive(n - 1)


def fibonacci_iterative(n):
    fibs = [0, 1]
    if n <= 1:
        return fibs[n]
    for i in range(n - 1):
        fibs.append(fibs[-2] + fibs[-1])
    return fibs[-1]


if __name__ == "__main__":
    print(fibonacci_iterative(1))
    print(fibonacci_iterative(2))
    print(fibonacci_iterative(5))
    print(fibonacci_iterative(10))
    print(fibonacci_recursive(1))
    print(fibonacci_recursive(2))
    print(fibonacci_recursive(5))
    print(fibonacci_recursive(10))
