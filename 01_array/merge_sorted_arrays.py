# crate a function to merge two sorted arrays so the results is also sorted

def merge_sorted_arrays(l1: list, l2: list) -> list:
    """
    Merge two sorted lists so the result is also sorted
    :param l1: input list 1
    :param l2: input list 2
    :return: merged list
    """
    # if any of the inputs is empty, return the other
    if not len(l1):
        return l2
    if not len(l2):
        return l1

    out = []
    l1_index, l2_index = 0, 0
    l1_item, l2_item = l1[l1_index], l2[l2_index]

    # merge
    while l1_item and l2_item:
        print(l1_item, l2_item)
        # merger
        if l1_item < l2_item:
            out.append(l1_item)
            l1_index += 1
            l1_item = l1[l1_index] if l1_index <= len(l1) - 1 else None
        else:
            out.append(l2_item)
            l2_index += 1
            l2_item = l2[l2_index] if l2_index <= len(l2) - 1 else None

    # after the merging there can be item left in one of the lists
    # add them at the end of the output as they have to belong there
    if l1_item:
        out.extend(l1[l1_index:])

    if l2_item:
        out.extend(l2[l2_index:])

    return out


if __name__ == "__main__":
    l1 = [2, 3, 6, 7, 10, 31]
    l2 = [4, 6, 30, 1000, 1001, 1002]
    print(f"Merging: {l1} and {l2}. Result: {merge_sorted_arrays(l1, l2)}")

    l1 = []
    l2 = [4, 6, 30, 1000, 1001, 1002]
    print(f"Merging: {l1} and {l2}. Result: {merge_sorted_arrays(l1, l2)}")
