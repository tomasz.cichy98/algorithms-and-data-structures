# create a function to reverse a string

def reverse_string(str_in: str) -> str:
    """
    Let's do it without using Python "cheats" like str.reverse() or list[::-1]
    :param str_in: string to reverse
    :return: reversed string
    """
    out = []
    for i in range(start=len(str_in) - 1, stop=-1, step=-1):
        out.append(str_in[i])
    return "".join(out)


if __name__ == "__main__":
    s_in = "hello"
    print(f"Reversed {s_in} is {reverse_string(s_in)}")
