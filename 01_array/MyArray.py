class MyArray:
    def __init__(self):
        self.length = 0
        self.data = {}

    def get(self, index: int):
        return self.data[index]

    def push(self, item):
        self.data[self.length] = item
        self.length += 1

    def pop(self):
        last_item = self.data[self.length - 1]
        del self.data[self.length - 1]
        self.length -= 1
        return last_item

    def delete(self, index: int):
        item = self.data[index]
        self._shift_items(index=index)
        return item

    def _shift_items(self, index: int):
        for i in range(index, self.length - 1):
            self.data[i] = self.data[i + 1]
        del self.data[self.length - 1]
        self.length -= 1


if __name__ == "__main__":
    my_array = MyArray()
    my_array.push("hello")
    my_array.push("hello again")
    my_array.push("hello for the third time")
    my_array.push("hello for the last time")
    print(f"Original array: {my_array.data}")
    my_array.pop()
    print(f"After pop: {my_array.data}")
    my_array.delete(1)
    print(f"After delete(1): {my_array.data}")
