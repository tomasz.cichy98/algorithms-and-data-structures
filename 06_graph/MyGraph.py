class MyGraph:
    def __init__(self):
        self.number_of_nodes = 0
        self.adj_list = {}

    def add_vertex(self, node):
        self.adj_list[node] = []
        self.number_of_nodes += 1

    def add_edge(self, node1, node2):
        # undirected
        self.adj_list[node1].append(node2)
        self.adj_list[node2].append(node1)

    def show_connections(self):
        pass


if __name__ == "__main__":
    my_graph = MyGraph()
    my_graph.add_vertex("0")
    my_graph.add_vertex("1")
    my_graph.add_vertex("2")
    my_graph.add_vertex("3")
    my_graph.add_vertex("4")
    my_graph.add_vertex("5")
    my_graph.add_edge("0", "1")
    my_graph.add_edge("0", "2")
    my_graph.add_edge("2", "1")
    my_graph.add_edge("3", "5")
    print(my_graph.adj_list)
