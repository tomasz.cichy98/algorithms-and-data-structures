from typing import Any


class Node:
    def __init__(self, value: Any):
        self.value = value
        self.next = None


class MySinglyLinkedList:
    def __init__(self, value: Any):
        self.head = Node(value=value)
        self.tail = self.head
        self.length = 1

    def append(self, value: Any):
        new_node = Node(value=value)
        self.tail.next = new_node
        self.tail = new_node
        self.length += 1

    def prepend(self, value: Any):
        new_node = Node(value=value)
        new_node.next = self.head
        self.head = new_node
        self.length += 1

    def insert(self, index: int, value: Any):
        if index >= self.length:
            self.append(value)
            return
        if index == 0:
            self.prepend(value)
            return

        new_node = Node(value=value)
        leader_node = self._traverse_to_index(index - 1)
        follower_node = leader_node.next
        leader_node.next = new_node
        new_node.next = follower_node
        self.length += 1

    def remove(self, index):
        leader_node = self._traverse_to_index(index - 1)
        unwanted_node = leader_node.next
        leader_node.next = unwanted_node.next
        del unwanted_node
        self.length -= 1

    def reverse(self):
        if not self.head.next:
            return self.head

        first = self.head
        self.tail = first
        second = self.head.next
        while second:
            temp = second.next
            second.next = first
            first = second
            second = temp
        self.head.next = None
        self.head = first

    def _traverse_to_index(self, index: int) -> Node:
        counter = 0
        current_node = self.head
        while counter != index:
            current_node = current_node.next
            counter += 1
        return current_node

    def __iter__(self):
        # a generator that return values in the linked list
        node = self.head
        while node:
            yield node
            node = node.next


def print_linked_list(linked_list: MySinglyLinkedList):
    for node in linked_list:
        print(node.value, end="")
        if node.next:
            print(" --> ", end="")
    print()


if __name__ == "__main__":
    head_value = 10
    my_singly_linked_list = MySinglyLinkedList(head_value)
    print_linked_list(my_singly_linked_list)
    my_singly_linked_list.append(9)
    my_singly_linked_list.append(8)
    print_linked_list(my_singly_linked_list)
    my_singly_linked_list.prepend(20)
    print_linked_list(my_singly_linked_list)

    my_singly_linked_list.insert(index=2, value=99)
    print_linked_list(my_singly_linked_list)
    my_singly_linked_list.insert(index=0, value=111)
    print_linked_list(my_singly_linked_list)
    my_singly_linked_list.insert(index=100, value=222)
    print_linked_list(my_singly_linked_list)

    my_singly_linked_list.remove(index=2)
    print_linked_list(my_singly_linked_list)

    my_singly_linked_list.reverse()
    print_linked_list(my_singly_linked_list)
