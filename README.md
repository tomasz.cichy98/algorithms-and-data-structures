# Algorithms and Data Structures

From scratch implementations of various algorithms and data structures using Python. 
Data structure implementations are simple, it is done to better understand them.

[Udemy Course](https://relxlearning.udemy.com/course/master-the-coding-interview-data-structures-algorithms/)

Certificate: In Progress
