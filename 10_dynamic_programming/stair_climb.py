from functools import lru_cache


# https://leetcode.com/problems/climbing-stairs
class Solution:
    def climbStairs(self, n: int) -> int:
        # return climb_recursive(0, n)
        return climb_bottom_up(n)


@lru_cache
def climb_recursive(start_stair: int, n_stairs: int) -> int:
    if start_stair > n_stairs:
        return 0
    if start_stair == n_stairs:
        return 1

    return climb_recursive(start_stair + 1, n_stairs) + climb_recursive(start_stair + 2, n_stairs)


def climb_bottom_up(n_stairs: int) -> int:
    if n_stairs == 0:
        return 0

    stairs = [1, 2]
    for i in range(2, n_stairs):
        stairs.append(stairs[i - 1] + stairs[i - 2])

    return stairs[n_stairs - 1]
