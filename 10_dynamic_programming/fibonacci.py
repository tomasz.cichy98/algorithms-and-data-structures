import time

# 0 1 1 2 3 5 8
from functools import lru_cache


def fibonacci_recursive(n):
    if n <= 1:
        return n
    return fibonacci_recursive(n - 2) + fibonacci_recursive(n - 1)


# 0 1 1 2 3 5 8
@lru_cache
def fibonacci_recursive_cached(n):
    if n <= 1:
        return n
    return fibonacci_recursive_cached(n - 2) + fibonacci_recursive_cached(n - 1)


def time_run(fn: callable, args):
    start_time = time.time()
    out = fn(*args)
    print(f"{fn.__name__} for args: {args} took {time.time() - start_time:.4f} seconds")
    return out


if __name__ == "__main__":
    print(time_run(fibonacci_recursive, args=[20]))
    print(time_run(fibonacci_recursive, args=[30]))

    print(time_run(fibonacci_recursive_cached, args=[20]))
    print(time_run(fibonacci_recursive_cached, args=[30]))
