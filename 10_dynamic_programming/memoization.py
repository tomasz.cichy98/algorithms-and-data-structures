from functools import lru_cache

my_cache = {}


def square_number(n: int) -> int:
    print(f"Calculating square of {n}")
    return n ** 2


@lru_cache
def cached_square_number(n: int) -> int:
    print(f"Calculating square of {n}")
    return n ** 2


def memoized_square_number(n: int):
    if n in my_cache:
        return my_cache[n]
    my_cache[n] = square_number(n)  # any operation here
    return my_cache[n]


if __name__ == "__main__":
    print(square_number(2))
    print(square_number(2))
    print(square_number(2))

    print(memoized_square_number(3))
    print(memoized_square_number(3))
    print(memoized_square_number(3))

    print(cached_square_number(4))
    print(cached_square_number(4))
    print(cached_square_number(4))
