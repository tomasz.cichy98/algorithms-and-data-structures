from typing import List


# https://leetcode.com/problems/house-robber/submissions/
class Solution:
    def __init__(self):
        self.cache = {}

    def rob(self, nums: List[int]) -> int:
        return max(self.rob_recursive(nums, start_index=0), self.rob_recursive(nums, start_index=1))

    def rob_recursive(self, nums: List[int], start_index: int):
        if start_index >= len(nums):
            return 0

        if start_index in self.cache:
            return self.cache[start_index]
        self.cache[start_index] = nums[start_index] + max(self.rob_recursive(nums, start_index + 2),
                                                          self.rob_recursive(nums, start_index + 3))
        return self.cache[start_index]
